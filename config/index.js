require('dotenv').config();

const pkg = require('../package.json');

const name = process.env.SERVICE_NAME || pkg.name;

module.exports = {
  name,
  host: process.env.SERVICE_HOST || '0.0.0.0',
  port: parseInt(process.env.SERVICE_PORT || '3000', 10),
  pidPath: process.env.PID_FILE_PATH || `var/run/${pkg.name}.pid`,
  logger: {
    name,
    level: process.env.LOG_LEVEL || 'trace'
  }
};
