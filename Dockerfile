FROM node:8.15.1

USER node

WORKDIR /home/node

COPY . .

RUN npm i --production --verbose

EXPOSE 3000

ENTRYPOINT ["node", "src/index.js"]
