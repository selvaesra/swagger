const express = require('express');

const YAML = require('yamljs');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');

const {
  setupRequestLogger,
  requestLogger,
  responseLogger,
  createLogger
} = require('@leif.nambara/express-logger');
const errorHandler = require('@leif.nambara/express-error-handler');
const notFoundHandler = require('@leif.nambara/express-not-found-handler');
const healthcheck = require('@leif.nambara/express-healthcheck');

const config = require('../config');

const swaggerYaml = fs.readFileSync('./docs/swagger.yaml', 'utf8');
const swaggerDocument = YAML.parse(swaggerYaml);

const logger = createLogger(config.logger);

logger.info({ event: 'config', config });

const app = express();

app.locals.config = config;
app.locals.logger = logger;

/* middlewares - pre routes */
app.use(setupRequestLogger);
app.use(requestLogger);
app.use(healthcheck);
/* middlewares - pre routes */

/* routes */
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
/* routes */

/* middlewares - post routes */
app.use(notFoundHandler);
app.use(errorHandler);
app.use(responseLogger);
/* middlewares - post routes */

app.listen(config.port, () => {
  logger.info(`${config.name} is listening on ${config.host}:${config.port}`);
});
